package database;

/**
 * A helper class, which contains default values
 */
public class DatabaseDefaults {
     final static String MONGODB_ADDRESS = "mongodb://mongo:27017";
     final static String DATABASE_NAME = "news-management";
     final static String NEWS_COLLECTION = "news";
     final static String RATES_COLLECTION = "rates";
     final static int TOP_LENGTH = 5;
     final static int UPDATE_INTERVAL = 300000;
}
