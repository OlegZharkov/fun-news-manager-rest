package database;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.UpdateOptions;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import javax.annotation.PostConstruct;

import javax.enterprise.context.ApplicationScoped;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static database.DatabaseDefaults.*;

/**
 * A class to interact with MongoDB
 */
@ApplicationScoped
public class MongodbConnector {

    private final Logger log = Logger.getLogger(this.getClass().getName());
    private MongoClient mongoClient;
    private HashMap<String, String> ratingsTop = new HashMap<>();
    private Timer timer;
    private int currentUpdateInterval = UPDATE_INTERVAL;

    /**
     * Callback to initialize mongo client and create cronjob to calculate top rated news
     */
    @PostConstruct
    public void init() {
        this.mongoClient = MongoClients.create(MONGODB_ADDRESS);
        createCronJob(UPDATE_INTERVAL);
    }

    /**
     * Import news from new rss feed
     * @param url
     * @throws IOException
     */
    public void importRSS(URL url) throws IOException {

        MongoCollection<Document> collection = this.mongoClient.getDatabase(DATABASE_NAME).getCollection(NEWS_COLLECTION);
        try {
            JSONArray news = readerToJsonArray(new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)));
            for (Object entry : news) {
                collection.insertOne(Document.parse(entry.toString()));
            }
        } catch (JSONException e) {
            System.err.println("News were not found on chanel: " + url);
            System.err.println("Please make sure, that URL is relevant!");
        }
    }

    /**
     * Get current news, so user can rate them
     * @return
     */
    public String getNews() {
        final MongoCollection<Document> collection = this.mongoClient.getDatabase(DATABASE_NAME).getCollection(NEWS_COLLECTION);
        return iterableToJsonArray(collection.find());
    }

    /**
     * Get top rated news from Database
     * @return
     */
    public String getTopRated() {
        final MongoCollection<Document> newsCollection = this.mongoClient.getDatabase(DATABASE_NAME).getCollection(NEWS_COLLECTION);
        BasicDBList dbl = new BasicDBList();
        for(String document: ratingsTop.keySet()){
            dbl.add(new ObjectId(document));
        }
        FindIterable<Document> result = newsCollection.find(new Document("_id", new Document("$in", dbl)));

        return iterableToJsonArray(result);
    }

    /**
     * Update top rated news list
     * @param topLength
     */
    public void updateTopRated(int topLength) {
        this.ratingsTop.clear();
        final MongoCollection<Document> ratingsCollection = this.mongoClient.getDatabase(DATABASE_NAME).getCollection(RATES_COLLECTION);

        final Document projectQuery = new Document();
        projectQuery.put("rateAvg", new BasicDBObject().append("$avg",new BsonString("$ratings") ));
        projectQuery.put("newsItemId", "$newsItemId");

        AggregateIterable<Document> aggregateIterable = ratingsCollection.aggregate(Arrays.asList(
                Aggregates.project(
                        projectQuery
                ),
                Aggregates.sort(new Document("rateAvg", -1)),
                Aggregates.limit(topLength)
        ));

        for(Document document:aggregateIterable){
            this.ratingsTop.put(document.get("newsItemId").toString(), document.get("rateAvg").toString());
        }
    }

    /**
     * creates new user rating
     * @param newsItemId
     * @param rating
     */
    public void putRating(String newsItemId, int rating) {
        final MongoCollection<Document> collection = this.mongoClient.getDatabase(DATABASE_NAME).getCollection(RATES_COLLECTION);

        final BasicDBObject id = new BasicDBObject();
        id.put("newsItemId", newsItemId);

        final BasicDBObject push = new BasicDBObject();
        push.put("$push", new BasicDBObject().append("ratings", rating));

        final UpdateOptions updateOptions = new UpdateOptions().upsert(true);
        collection.updateMany(id, push, updateOptions);
    }

    /**
     *  Changes cron update interval
     * @param interval milliseconds
     */
    public void changeUpdateInterval(int interval) {
        this.timer.cancel();
        createCronJob(interval);
    }

    /** Check if database exists
     * https://stackoverflow.com/questions/31909247/mongodb-3-java-check-if-collection-exists
     * @return
     */
    public boolean isInitialized(){
        return this.mongoClient.getDatabase(DATABASE_NAME).listCollectionNames()
                .into(new ArrayList<>()).contains(NEWS_COLLECTION);
    }

    /*
    Utils
     */

    /**
     * Extracts <item> objects from RSS and converts it to JSON
     * @param reader
     * @return
     * @throws JSONException
     */
    public JSONArray readerToJsonArray(Reader reader) throws JSONException {
        JSONObject rootJson = XML.toJSONObject(reader);
        JSONObject rss = (JSONObject) rootJson.get("rss");
        JSONObject channel = (JSONObject) rss.get("channel");
        return channel.getJSONArray("item");
    }
    /** Convert Iterable object to JSON Array
     * initial idea is from https://stackoverflow.com/questions/52169070/is-there-a-way-to-convert-a-finditerabledocument-into-jsonarray-string
     * @param documents
     * @return
     */
    private String iterableToJsonArray(Iterable<Document> documents){
       return StreamSupport.stream(documents.spliterator(), false)
                .map(Document::toJson).collect(Collectors.joining(", ", "[", "]"));
    }

    void createCronJob(int interval){
        this.currentUpdateInterval = interval;
        this.timer = new Timer();
        TimerTask mTask = new TimerTask() {
            @Override
            public void run() {
                updateTopRated(TOP_LENGTH);
                log.info(ratingsTop.toString());
            }
        };

        this.timer.scheduleAtFixedRate(mTask, 0, interval);
    }

    /**
     *  Setters & Getters
     */
    public int getCurrentUpdateInterval() {
        return currentUpdateInterval;
    }
}