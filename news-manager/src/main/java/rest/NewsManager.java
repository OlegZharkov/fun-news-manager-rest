package rest;

import database.MongodbConnector;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;

/**
 * Restful API to get or to import news entries from the database.
 */
@Path("/news")
public class NewsManager {

    @Inject
    private MongodbConnector dbConnector;

    /**
     * Get news to be rated
     * @return news Json
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getNews() {
        return dbConnector.getNews();
    }

    /**
     * Import news from RSS URL
     * @param urlJson a Json object with contains RSS URL
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void importNewRssFeed(String urlJson) throws IOException {
        String rssUrl = Document.parse(urlJson).get("rssUrl").toString();
        dbConnector.importRSS(new URL(rssUrl));
    }
}