package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Helper class, which defines REST endpoint
 */
@ApplicationPath("/rest")
public class JaxRsActivator extends Application { }