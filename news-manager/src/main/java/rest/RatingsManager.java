package rest;

import database.MongodbConnector;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Restful API to manage user ratings
 */
@Path("/ratings")
public class RatingsManager {

    @Inject
    private MongodbConnector dbConnector;

    /**
     * get first 5 top rated news
     * @return JSON
     */
    @GET
    @Path("top-rated")
    @Produces(MediaType.APPLICATION_JSON)
    public Document getTop5() {
        return new Document("ratings", dbConnector.getTopRated())
                .append("updateInterval", dbConnector.getCurrentUpdateInterval());
    }

    /**
     * create new rating entry
     * @param newsItemId news entry ID
     * @param ratingValue rating Value
     */
    @PUT
    @Path("/{newsItemId}/{ratingValue}")
    @Produces(MediaType.APPLICATION_JSON)
    public void createNewsEntry(@PathParam("newsItemId") String newsItemId,
                                @PathParam("ratingValue") int ratingValue) {
        dbConnector.putRating(newsItemId, ratingValue);
    }

    /**
     * Change update interval for most top rated values
     * @param intervalMillis interval period in milliseconds
     * @throws IOException
     */
    @POST
    @Path("/change-cron-interval/{intervalMillis}")
    public void changeCronInterval(@PathParam("intervalMillis") int intervalMillis) throws IOException {
        dbConnector.changeUpdateInterval(intervalMillis);
    }

}
