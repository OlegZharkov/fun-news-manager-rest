package startup;

import database.MongodbConnector;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;

@Startup
@Singleton
public class StartupBean {
    // default RSS urls, specified in the task
    private final String[] defaultRssFeeds = {
            "https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml",
            "https://rss.nytimes.com/services/xml/rss/nyt/Europe.xml"
    };

    @Inject
    private MongodbConnector dbConnector;

    /**
     * Import default 
     */
    @PostConstruct
    public void init() {
        try {
            if (!dbConnector.isInitialized()) {
                for (String feed : defaultRssFeeds) {
                    dbConnector.importRSS(new URL(feed));
                }
            }
        } catch (IOException e){
            System.err.println("Initial import has failed! Please check RSS availability!");
            e.printStackTrace();
        }
    }
}