import com.mongodb.client.FindIterable;
import database.MongodbConnector;
import org.bson.Document;
import org.json.JSONArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class UtilitiesTester {

    @Test
    public void testIteratorToJsonArray() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        MongodbConnector mongodbConnector = new MongodbConnector();

        Method method = MongodbConnector.class.getDeclaredMethod("iterableToJsonArray", Iterable.class);
        method.setAccessible(true);
        List<Document> list = new ArrayList<>();
        list.add(new Document("JsonKey", "JsonValue"));

        String output = (String) method.invoke(mongodbConnector, list);
        Assertions.assertEquals("[{ \"JsonKey\" : \"JsonValue\" }]", output);
    }

    @Test
    public void testReaderToJsonArray() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, IOException {
        MongodbConnector mongodbConnector = new MongodbConnector();

        Method method = MongodbConnector.class.getDeclaredMethod("readerToJsonArray", Reader.class);
        method.setAccessible(true);

        URL fileToRead = UtilitiesTester.class.getResource("rss.xml");

        JSONArray news = (JSONArray) method.invoke(mongodbConnector,
                new BufferedReader(new InputStreamReader(fileToRead.openStream(), StandardCharsets.UTF_8)));

        Assertions.assertEquals(20, news.length());
    }
}
