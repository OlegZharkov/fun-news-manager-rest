#!/bin/bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
mvn clean package -f news-manager/
pushd ../fun-news-manager-front/ && ng build && popd
docker-compose up
